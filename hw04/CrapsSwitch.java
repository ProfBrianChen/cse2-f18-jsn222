//Jackson Nichols-Worley
//September 25th, 2018
//CSE 02 Craps Switch: 

//imports random class
import java.util.Random;
//imports scanner class
import java.util.Scanner;

public class CrapsSwitch {
  public static void main (String args[]){
    
    //creates random generaor object
    Random randomGenerator = new Random();
    //creates scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //prompts the user to choose between random numbers or inputting numbers
    System.out.println("Would you like to randomly cast dice? Enter yes or no.");
    
    //declares and initializes variable for user's choice
    String choice = myScanner.next();
    
    //declares variables for dice
    int die1 = 0;
    int die2 = 0;
    
    //switch statement to determine whether user wants to get random numbers or to input numbers
    switch (choice){
      //if the user inputs "yes", die1 and die2 are set to random numbers between 1 and 6
      case "yes":
          //chooses a random number between 0 and 5 and then adds 1
          die1 = randomGenerator.nextInt(5) + 1;
          die2 = randomGenerator.nextInt(5) + 1;
          break;
      //if the user inputs "no", die1 and die2 are set to numbers that the user inputs
      case "no":
           System.out.println("Please enter a number from 1 to 6");
           die1 = myScanner.nextInt();
        //checks whether user input is in the range of 1 to 6, does nothing if it is, prompts user
        //to input another number if it isn't, while loop repeats process until valid number is inputted
        while (die1 < 1 || die1 > 6){
           switch (die1){
             case 1:
             case 2:
             case 3:
             case 4:
             case 5:
             case 6: 
                  break;
             default: System.out.println("You entered an invalid number, please enter another number.");
                      die1 = myScanner.nextInt();
                      break;
                  }   
        }
           System.out.println("Please enter another number from 1 to 6");
           die2 = myScanner.nextInt();
        while (die2 < 1 || die2 > 6){
           switch (die2){
             case 1:
             case 2:
             case 3:
             case 4:
             case 5:
             case 6: 
                  break;
             default: System.out.println("You entered an invalid number, please enter another number.");
                      die2 = myScanner.nextInt();
                      break;
                   }
        }
            break;       
    }
    
    //switch statement for die1 to test all 6 roll possibilities
    switch (die1){
      case 1:
        //nested switch statement for die2 to test all 6 roll possibility with each roll possibility of
        //die1, repeats for each case and prints matching statement to corresponding roll
          switch (die2){
            case 1: 
                System.out.println("You rolled snake eyes.");
                break;    
            case 2:
                 System.out.println("You rolled an ace deuce.");
                break;                
            case 3:
                System.out.println("You rolled an easy four.");
                break;                
            case 4:
                System.out.println("You rolled a fever five.");
                break;                
            case 5:
                System.out.println("You rolled an easy six.");
                break;                
            case 6:
                System.out.println("You rolled a seven out.");
                break;  
          }
          break;
      case 2:
          switch (die2){
            case 1: 
                System.out.println("You rolled an ace deuce.");
                break;    
            case 2:
                 System.out.println("You rolled a hard four.");
                break;                
            case 3:
                System.out.println("You rolled a fever five.");
                break;                
            case 4:
                System.out.println("You rolled an easy six.");
                break;                
            case 5:
                System.out.println("You rolled a seven out.");
                break;                
            case 6:
                System.out.println("You rolled an easy eight.");
                break;
        }
          break;
      case 3:
          switch (die2){
            case 1: 
                System.out.println("You rolled an easy four.");
                break;    
            case 2:
                 System.out.println("You rolled a fever five.");
                break;                
            case 3:
                System.out.println("You rolled a hard six.");
                break;                
            case 4:
                System.out.println("You rolled a seven out.");
                break;                
            case 5:
                System.out.println("You rolled an easy eight.");
                break;                
            case 6:
                System.out.println("You rolled a nine.");
                break;
        }
          break;        
      case 4:
          switch (die2){
            case 1: 
                System.out.println("You rolled a fever five.");
                break;    
            case 2:
                 System.out.println("You rolled an easy six.");
                break;                
            case 3:
                System.out.println("You rolled a seven out.");
                break;                
            case 4:
                System.out.println("You rolled a hard eight.");
                break;                
            case 5:
                System.out.println("You rolled a nine.");
                break;                
            case 6:
                System.out.println("You rolled an easy ten.");
                break;
        }
          break;       
      case 5:
          switch (die2){
            case 1: 
                System.out.println("You rolled an easy six.");
                break;    
            case 2:
                 System.out.println("You rolled a seven out.");
                break;                
            case 3:
                System.out.println("You rolled an easy eight.");
                break;                
            case 4:
                System.out.println("You rolled a nine.");
                break;                
            case 5:
                System.out.println("You rolled a hard ten.");
                break;                
            case 6:
                System.out.println("You rolled a yo-leven.");
                break;
        }
          break;        
      case 6:
          switch (die2){
            case 1: 
                System.out.println("You rolled a seven out.");
                break;    
            case 2:
                 System.out.println("You rolled an easy eight.");
                break;                
            case 3:
                System.out.println("You rolled a nine.");
                break;                
            case 4:
                System.out.println("You rolled an easy ten.");
                break;                
            case 5:
                System.out.println("You rolled a yo-leven.");
                break;                
            case 6:
                System.out.println("You rolled boxcars.");
                break;
        }
          break;        
        
    }
  }
}