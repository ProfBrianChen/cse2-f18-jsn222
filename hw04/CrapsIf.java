//Jackson Nichols-Worley
//September 25th, 2018
//CSE 02 Craps If: 

//imports random class
import java.util.Random;
//imports scanner class
import java.util.Scanner;

public class CrapsIf {
  public static void main (String args[]){
    
    //creates random generaor object
    Random randomGenerator = new Random();
    //creates scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //prompts the user to choose between random numbers or inputting numbers
    System.out.println("Would you like to randomly cast dice? Enter yes or no.");
    
    //declares and initializes variable for user's choice
    String choice = myScanner.next();
    
    //declares variables for dice
    int die1;
    int die2;
    
    //if and else if statements to determine whether user wants to get random numbers or to input numbers
    //if the user inputs "yes", die1 and die2 are set to random numbers between 1 and 6
    if (choice.equals("yes")){
      //chooses a random number between 0 and 5 and then adds 1      
      die1 = randomGenerator.nextInt(5) + 1;
      die2 = randomGenerator.nextInt(5) + 1;
    }
    //if the user inputs "no", die1 and die2 are set to numbers that the user inputs
    else if (choice.equals("no")){
      System.out.println("Please enter a number from 1 to 6");
      die1 = myScanner.nextInt();
      //checks whether user input is in the range of 1 to 6, does nothing if it is, prompts user
      //to input another number if it isn't, while loop repeats process until valid number is inputted
      while (die1 < 1 || die1 > 6){
        System.out.println("You entered an invalid number, please enter another number.");
        die1 = myScanner.nextInt();
      }
      System.out.println("Please enter another number from 1 to 6");
      die2 = myScanner.nextInt();
      while (die2 < 1 || die2 > 6){
        System.out.println("You entered an invalid number, please enter another number.");
        die2 = myScanner.nextInt();
      }
    }
    else{
      System.out.println("Invalid choice.");
      die1 = 0;
      die2 = 0;
    }
    
    //if statements for different possible rolls
    //when both numbers are the same, don't need to check twice for each die
    if (die1 == 1 && die2 == 1){
      System.out.println("You rolled snake eyes.");
    }
    //when numbers are different, need to check twice using or conditional for each die
    else if (die1 == 1 && die2 == 2 || die1 == 2 && die2 == 1){
      System.out.println("You rolled an ace deuce.");
    }
    else if (die1 == 1 && die2 == 3 || die1 == 1 && die2 == 1){
      System.out.println("You rolled an easy four.");
    }  
    //when multiple rolls have the same term, can use or conditional to check for multiple types of rolls 
    else if (die1 == 1 && die2 == 4 || die1 == 4 && die2 == 1 || 
             die1 == 2 && die2 == 3 || die1 == 3 && die2 == 2){
      System.out.println("You rolled a fever five.");
    }    
    else if (die1 == 1 && die2 == 5 || die1 == 5 && die2 == 1 || 
             die1 == 2 && die2 == 4 || die1 == 4 && die2 == 2){
      System.out.println("You rolled an easy six.");
    }
    else if (die1 == 1 && die2 == 6 || die1 == 6 && die2 == 1 || 
             die1 == 2 && die2 == 5 || die1 == 5 && die2 == 2 ||
             die1 == 3 && die2 == 4 || die1 == 4 && die2 == 3){
      System.out.println("You rolled a seven out.");
    }    
    else if (die1 == 2 && die2 == 2){
      System.out.println("You rolled a hard four.");
    }
    else if (die1 == 2 && die2 == 6 || die1 == 6 && die2 == 2 || 
             die1 == 3 && die2 == 5 || die1 == 5 && die2 == 3){
      System.out.println("You rolled an easy eight.");
    }    
    else if (die1 == 3 && die2 == 3){
      System.out.println("You rolled a hard six.");
    }
    else if (die1 == 3 && die2 == 6 || die1 == 6 && die2 == 3 || 
             die1 == 4 && die2 == 5 || die1 == 5 && die2 == 4){
      System.out.println("You rolled a nine.");
    }      
    else if (die1 == 4 && die2 == 4){
      System.out.println("You rolled a hard eight.");
    }
    else if (die1 == 4 && die2 == 6 || die1 == 6 && die2 == 4){
      System.out.println("You rolled an easy ten.");
    }    
    else if (die1 == 5 && die2 == 5){
      System.out.println("You rolled a hard ten.");
    }
    else if (die1 == 5 && die2 == 6 || die1 == 6 && die2 == 5){
      System.out.println("You rolled a yo-leven.");
    }    
    else if (die1 == 6 && die2 == 6){
      System.out.println("You rolled boxcars.");
    }     
  }
}