//imports Arrays class
import java.util.Arrays; 
//imports java class
import java.util.Random;

public class Lab08 {
  public static void main (String args[]){
    
    //creates random object
    Random generator = new Random();
    
    //creates two int arrays each with a size of 100
    int[] array1 = new int[100]; 
    int[] array2 = new int[100]; 
    
    //for loop that iterates 100 times
    for (int i = 0; i <100; i++){
      //int that generates a random number between 1 and 100
      int num = generator.nextInt(100);
      
      //sets first array number at i as the random number
      array1[i] = num;
      //increments the second array at the random number to keep track of how times it occurs
      array2[num]++;
    }
    System.out.println(Arrays.toString(array1));
    System.out.println(Arrays.toString(array2));
  }
}