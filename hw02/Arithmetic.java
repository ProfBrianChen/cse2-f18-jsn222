//Jackson Nichols-Worley
//September 11th, 2018
//CSE 02 Arithmetic Calculations: Given price and amount inputs of clothing items and the sales tax,
//find the amount of money spent on the purchase
public class Arithmetic {
  public static void main (String args[]){
    //input variables
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltPrice = 33.99;
    //the tax rate
    double paSalesTax = 0.06;

    //output variable declarations
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts, salesTaxChargedOnPants, salesTaxChargedOnShirts,
    salesTaxChargedOnBelts, totalCostofPurchase, totalSalesTax, totalPaidForTransaction;
    
    //finding total cost before tax: multiply number of items by the amount each item costs
    //pants
    totalCostOfPants = numPants * pantsPrice;
    System.out.println ("The total cost of the pants before tax is: " + totalCostOfPants);
    //shirts
    totalCostOfShirts = numShirts * shirtPrice;
    System.out.println ("The total cost of the shirts before tax is: " + totalCostOfShirts);
    //belts
    totalCostOfBelts = numBelts * beltPrice;
    System.out.println ("The total cost of the belts before tax is: " + totalCostOfBelts);
    
    //finding sales tax charged: multiply total cost of item by the sales tax
    //get rid of extra decimal places by multiplying by 100, casting to an int and dividing by 100.0, 
    //only have to do on original sales tax
    //pants
    salesTaxChargedOnPants = totalCostOfPants * paSalesTax;
    salesTaxChargedOnPants = (int)(salesTaxChargedOnPants * 100)/100.0;
    System.out.println ("The sales tax charged on the pants is: " + salesTaxChargedOnPants);
    //shirts
    salesTaxChargedOnShirts = totalCostOfShirts * paSalesTax;
    salesTaxChargedOnShirts = (int)(salesTaxChargedOnShirts * 100)/100.0;
    System.out.println ("The sales tax charged on the shirts is: " + salesTaxChargedOnShirts);
    //belts
    salesTaxChargedOnBelts = totalCostOfBelts * paSalesTax;
    salesTaxChargedOnBelts = (int)(salesTaxChargedOnBelts * 100)/100.0;
    System.out.println ("The sales tax charged on the belts is: " + salesTaxChargedOnBelts);
    
    //finding total cost of purchace before tax: add total cost of each item
    totalCostofPurchase = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    System.out.println ("The total cost of the purchase is: " + totalCostofPurchase);
    
    //finding total sales tax: add sales tax charged of each item
    totalSalesTax = salesTaxChargedOnPants + salesTaxChargedOnShirts + salesTaxChargedOnBelts;
    System.out.println ("The total sales tax is: " + totalSalesTax);
    
    //finding total paid for transaction: add total cost of purchase and total sales tax
    totalPaidForTransaction = totalCostofPurchase + totalSalesTax;
    System.out.println ("The total paid for the transaction is: " + totalPaidForTransaction);
  }
}
  
  