//imports Arrays class
import java.util.Arrays; 
//imports java class
import java.util.Random;
//imports scanner class
import java.util.Scanner;

public class RemoveElements {
  public static void main(String [] arg){
    Scanner scan=new Scanner(System.in);
    int num[]=new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer="";
    do{
      System.out.print("Random input 10 ints [0-9]");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      
      System.out.print("Enter the index ");
      index = scan.nextInt();
      newArray1 = delete(num,index);
      String out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
      
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2="The output array is ";
      out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
      
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer=scan.next();
    }while(answer.equals("Y") || answer.equals("y"));
  }
  
  public static String listArray(int num[]){
    String out="{";
    for(int j=0;j<num.length;j++){
      if(j>0){
        out+=", ";
      }
      out+=num[j];
    }
    out+="} ";
    return out;
  }
  
  public static int[] randomInput(){
    int[] list = new int[10];
    
    Random generator = new Random();
    
    for (int i = 0; i < list.length; i++){
      //generates a random number from 0 to 9 and sets it for each spot in the array
      int num = generator.nextInt(10);
      list[i] = num;
    }
    
    return list;
  }
  
  public static int[] delete(int[] list, int pos){
    //checks whether the index is in the bounds of the array
    if (pos < 0 || pos > 9){
      System.out.println("The index is not valid.");
      return list;
    }
    
    int[] newList = new int[list.length - 1];
    
    for (int i = 0; i < newList.length; i++){
      //if the spots in the array are less than the index being removed, copies from the originial array
      if (i < pos){
        newList[i] = list[i];
      }
      //if the spots in the array are greater than the index being removed, copies from the next highest spot in the 
      //array to account for the removed index
      else if (i > pos){
        newList[i] = list[i + 1];
      }
    }
    
    return newList;
  }
  
  public static int[] remove(int [] list, int target){
    int count = 0;
    
    //counts the number of times the target appears in the array
    for (int i = 0; i < list.length; i++){
      if (list[i] == target){
        count++;
      }
    }
    
    //if the target does not appear in the array, the original array is returned 
    if (count == 0){
      System.out.println("Element " + target + " was not found.");
      return list;
    }
    
    //creates an array that is as long as the original array minus the number of times the target appears
    int[] newList = new int[list.length - count];
    int index = 0;
    
    //runs through the original list length
    for (int i = 0; i < list.length; i++){
      //if the number in the array at each spot does not equal the target number, the newList at the index number is 
      //set to the list at the number of the iteration, the index increases only when the number is not equal to the 
      //target, effectively removing the target number(s)
      if (list[i] != target){
        newList[index] = list[i];
        index++;
      }
    }
    
    System.out.println("Element " + target + " was found.");
    return newList;
  }
}
