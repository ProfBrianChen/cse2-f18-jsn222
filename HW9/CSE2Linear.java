//imports Arrays class
import java.util.Arrays; 
//imports java class
import java.util.Random;
//imports scanner class
import java.util.Scanner;

public class CSE2Linear {
  public static void main (String args[]){
    Scanner scan = new Scanner(System.in);
    
    //empty int array to hold 15 ints for grades
    int [] grades = new int[15];
    
    System.out.println("Enter 15 ascending ints for final grades: ");
    
    int test = 0;
    
    //for loop, runs through grades
    for (int i = 0; i < grades.length; i++){
      //sets variable as next the int each time through
      test = scan.nextInt();
      //checks if int is in range
      if (test >=0 && test<=100){
        //checks if int is greater than or equal to previous, does nothing for first number
        if (i != 0){
          if(test >= grades[i-1]){
            //sets next int as the next spot in the array
            grades[i] = test;
          }
          else{
            System.out.println("The number entered is not greater than or equal to the previous one.");
            i--;
          }
        }
        else{
          grades[i] = test;
        }
      }
      else{
        System.out.println("The number entered is not in the range.");
        i--;
      }
    }
    
    System.out.println(Arrays.toString(grades));
    
    
    int wantedGrade;
    System.out.println("Enter a grade to be searched for: ");
    wantedGrade = scan.nextInt();

    binarySearch(grades, wantedGrade);
    
    System.out.println("Scrambled: ");
    int [] shuffled = scramble(grades);
    System.out.println(Arrays.toString(shuffled));
    
    int wantedGrade2;
    System.out.println("Enter a grade to be searched for: ");
    wantedGrade2 = scan.nextInt();
    
    linearSearch(shuffled, wantedGrade2);
  }
  
  public static int[] scramble (int[] list){
    Random generator = new Random();
    
    int[] updatedList;
    
    //iterates 1000 times to increase randomness
    for (int i = 0; i < 1000; i++){
      //generators number between 1 and 15
      int num = generator.nextInt(14) + 1;
      
      //switches first int with int at whatever position was generated
      int temp = list[0];
      list [0] = list[num];
      list[num] = temp;
    }
    
    updatedList = list.clone();
    return updatedList;
  }
  
  public static void linearSearch(int[] list, int search){
    int iterations = 0;
    boolean found = false;
    
    //runs through entire array
    for (int i = 0; i < list.length; i++){
      //if the desired number is found, the boolean variable is set to true and the loop breaks
      if (search == list[i]){
        found = true;
        break;
      }
      //if the number is not found, increases the count of the number of iterations
      iterations++;
    }
    
    //print statements for whether the number was found or not
    if (found == false){
      System.out.println(search + " was not found in the list with " + iterations + " iterations.");
    }
    else{
      System.out.println(search + " was found in the list with " + iterations + " iterations.");
    }
  }
  
  public static void binarySearch(int[] list, int search){
    //variables for different spots in the array
    int low = 0;
    int high = list.length - 1;
    int mid;
    
    int iterations = 0;
    boolean found = false;
    
    while (high >= low){
      //sets the mid as the middle of whatever numbers are currently being tested
      mid = (high + low)/2;
      //if the number is found the boolean variable is set to true and the loop breaks
      if (list[mid] == search){
        found = true;
        break;
      }
      //if the number is less than the middle or greater than the middle, the bounds change accordingly and the process resets
      else if (list[mid] < search){
        low = mid + 1;
        iterations++;
      }
      else if(list[mid] > search){
        high = mid - 1;
        iterations++;
      }
    }
    
    //print statements for whether the number was found or not
    if (found == false){
      System.out.println(search + " was not found in the list with " + iterations + " iterations.");
    }
    else{
      System.out.println(search + " was found in the list with " + iterations + " iterations.");
    }
  }
}