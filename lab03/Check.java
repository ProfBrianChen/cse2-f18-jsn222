//Jackson Nichols-Worley
//September 14th, 2018
//CSE 02 Check: Takes data for an amount of money owed. The program should take user input for the amount owed,
//how much the user wants the tip to be, and how many people are paying, and then outputs how much each person owes

//imports scanner class, not built in to program
import java.util.Scanner;

public class Check {
  public static void main (String args[]){
    //creates new scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //prompts the user to input the cost of the check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    //creates variable for the cost of the check and stores information                 
    double checkCost = myScanner.nextDouble();  
    
    //prompts the user to input tip in percent form
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    //creates variable for tip and stores information
    double tipPercent = myScanner.nextDouble();
    //converts the tip to decimal form 
    tipPercent /= 100; 
      
    //prompts the user to input number of people who are paying
    System.out.print("Enter the number of people who went out to dinner: ");
    //creates variavle for people paying and stores information
    int numPeople = myScanner.nextInt();
    
    //total cost of check
    double totalCost;
    //amount each person owes
    double costPerPerson; 
    //takes whole dollar amount of cost
    int dollars, 
    //takes decimal part of cost
    dimes, pennies;
    //multiplies cost of check by the tip percent plus 1 to get the total cost
    totalCost = checkCost * (1 + tipPercent);
    //divides the total cost by the number of people
    costPerPerson = totalCost / numPeople;
    //gets the dollar amount of the cost, dropping the decimal
    dollars=(int)costPerPerson;
    //gets dimes amount of the cost, uses mod operator to get remainder representing it
    dimes=(int)(costPerPerson * 10) % 10;
    //gets pennies amount of the cost, uses mod operator to get remainder representing it
    pennies=(int)(costPerPerson * 100) % 10;
    //prints out how much each person owes
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);  
  }
}