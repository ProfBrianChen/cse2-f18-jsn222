//Jackson Nichols-Worley
//September 7th, 2018
//CSE 02 Cyclometer: Takes data for two bicycle trips, time elapsed in seconds and number of rotations during
//that time. The program should convert the data into time elapsed in minutes and miles traveled using arithmetic.
public class Cyclometer {
  public static void main (String args[]){
    //input variables
    int secsTrip1=480; //seconds trip 1 took
   	int secsTrip2=3220; //seconds trip 2 took
		int countsTrip1=1561; //counts wheel rotated during trip 1
		int countsTrip2=9037; //counts wheel rotated during trip 2
    
    //constant variables
    double wheelDiameter=27.0, //diameter of wheel
  	PI=3.14159, //Pi, used for calculations involving circles
  	feetPerMile=5280, //conversion rate for feet to miles
  	inchesPerFoot=12, //conversion rate for inches to feet
  	secondsPerMinute=60; //conversion rate for seconds to minutes
    
    //output variables
    double distanceTrip1, distanceTrip2, totalDistance;
    
    //print the time each trip took, use second to minute conversion rate
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    
    //calculate distance traveled
    //multiply each tire roation times the wheel diameter multiplied by pi to find the circumference of the wheel
    distanceTrip1 = countsTrip1 * wheelDiameter*PI;
    //divide the circumference by the conversion rate for inches to feet multiplied by the conversion rate for feet to miles
	  distanceTrip1 /= inchesPerFoot * feetPerMile;
	  distanceTrip2 = countsTrip2 * wheelDiameter*PI;
    distanceTrip2 /= inchesPerFoot * feetPerMile;
	  totalDistance = distanceTrip1 + distanceTrip2;
    
    //print the distance traveled
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");
  }
}