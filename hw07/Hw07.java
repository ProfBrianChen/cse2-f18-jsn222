//imports scanner class
import java.util.Scanner;

class Hw07{
  
  public static void main (String [] args){
    //String variable, calls sample text method and stores input
    String outputText = sampleText();
    System.out.println("You entered: " + outputText);
    System.out.println();
    
    //scanner object
    Scanner userInput = new Scanner (System.in);
    
    String choice = "test";
    boolean value = false;
    
    //while loop, calls print menu while value is false, value is false unless user inputs one of the given
    //chars in the menu
    while (value == false){
      //calls printMenu variable
      printMenu();
      
      //sets choie variable as user input
      choice = userInput.next();
      
      //checks whether user input is one of the given chars
      if (choice.equals("test") || choice.equals("c") || choice.equals("w") || choice.equals("f") || choice.equals("r")
            || choice.equals("s") || choice.equals("q")){
        value = true;
      } 
    }
    
    //ends the program if user enters q
    if (choice.equals("q")){
      return;
    }
    //calls number of white space method if user enters c
    else if(choice.equals("c")){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(outputText));
    }
    //calls number of words method if user enters w
    else if(choice.equals("w")){
      System.out.println("Number of words: " + getNumOfWords(outputText));
    }
    //calls find text method if user enters f    
    else if(choice.equals("f")){
      //prompts user to enter a word
      System.out.println("Enter a word or phrase to be found:");
      String sampleText = userInput.next();
      System.out.println(sampleText + " instances: " + findText(outputText, sampleText));
    }
    //calls replace exclamation method if user enters r 
    else if(choice.equals("r")){
      System.out.println("Edited text: " + replaceExclamation(outputText));
    }
    //calls shorten space method if user enters s    
    else if(choice.equals("s")){
      System.out.println("Edited text: " + shortenSpace(outputText));
    } 
  }
  
  //sample text method
  public static String sampleText(){
    String outputText;
    Scanner userInput = new Scanner (System.in);
    
    //promts the user to input text
    System.out.println("Enter a sample text.");
    //saves the input
    outputText = userInput.nextLine();

    return outputText;
  }
  
  //print menu method
  public static void printMenu(){
    
    //prints out the menu
    System.out.println("Menu");
    System.out.println("c - Number of non-whitespace characters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all !'s");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println();
    System.out.println("Choose an option:");
  }
  
  //number of white space characters 
  public static int getNumOfNonWSCharacters(String text){
    //counter variable
    int counter = 0;
    
    //for loop, runs through chars in text
    for (int i = 0; i < text.length(); i++){
      //adds to counter if there is a space at i
      if (text.charAt(i) != ' '){
        counter++;
      }
    }
    return counter;
  }
  
  //get number of words method
  public static int getNumOfWords(String text){
    //counter variable, starts at 1 to account for last word
    int counter = 1;
    
    //for loop that runs through chars in text
    for (int i = 0; i < text.length(); i++){
      //adds to counter after every space, there is a space after each new word
      if (text.charAt(i) == ' '){
        counter++;
      }
    }
    return counter;
  }
  
  //find text method
  public static int findText(String text, String sampleText){
    //counter variable, calls splt method which finds where sample text is in the
    //text and splits the text, puts new strings in an array, gets length of array
    int counter = text.split(sampleText + " ").length;

    return counter;
  }
        
  //replace exclamation method
  public static String replaceExclamation(String text){
    //string variable that can be altered
    String newText = text;
    //for loop that runs through chars in text
    for (int i = 0; i < newText.length(); i++){
      //if the char is an exclamation point, takes a substring from the begining of 
      //the string to the char, adds a period, and takes a substring from the next char to the end
      if (text.charAt(i) == '!'){
        newText = newText.substring(0, i) + '.' + newText.substring(i + 1);
      }
    }
    return newText;
  }
  
  //shorten space method
  public static String shortenSpace(String text){
    String newText = text;
    //for loop that runs through chars in text
    for (int i = 0; i < newText.length(); i++){
      //if char and char after it are both spaces, removes one of the spaces by taking substrings around it
      if (newText.charAt(i) == ' '){
        if (newText.charAt(i+1) == ' '){
          newText = newText.substring(0, i) + newText.substring(i + 1);
        }
      }
    }
    return newText;
  }  
}