//Jackson Nichols-Worley
//October 9th, 2018
//CSE 02 Poker Probability: 

//imports scanner class, not built in to program
import java.util.Scanner;
//imports random class
import java.util.Random;
//imports array list class
import java.util.ArrayList;
//imports decimal format class
import java.text.DecimalFormat;

public class Hw05 {
  public static void main (String args[]){
    //creates new scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //creates new random object
    Random randomGenerator = new Random();
    
    //boolean variable for testing the input
    boolean goodInput;
    
    //prompts the user to input a value
    System.out.println("How many hands would you like to generate?");
    
    int numberOfHands;
    
    goodInput = myScanner.hasNextInt();
    
    //while loop that continually prompts the user for an integer value
    while (!goodInput){
      System.out.println("Please enter an integer for the number of hands.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }
    
    //assigns numberOfHands once user has inputed an integer
    numberOfHands = myScanner.nextInt();
    
    //int variables for 5 cards
    int card1, card2, card3, card4, card5;
    //int variables for types of hands
    int fourOfAKind = 0, threeOfAKind = 0, twoPair = 0, onePair = 0;
    
    //array list for deck of cards
    ArrayList<Integer> cards = new ArrayList<Integer>();
    ArrayList<Integer> removableCards = new ArrayList<Integer>();
    
    //for loop to fill array list with 52 cards
    for (int i = 1; i <= 52; i++){
      cards.add(i);
    }
    
    //for loop to run through number of hands user inputed
    for (int i = 0; i < numberOfHands; i++){
      //array list for cards in each hand, gets reset each time the loop is executed
      ArrayList<Integer> cardHand = new ArrayList<Integer>();
      
      //sets removable cards array list as cards array list, allows cards to be removed from the array
      //each time while still keeping the origial cards array list the same
      removableCards = new ArrayList<Integer>(cards);
      
      //assigns card1 to a random number in the array list of cards
      card1 = removableCards.get(randomGenerator.nextInt(removableCards.size()));
      //removes card1 from the removable cards array list
      removableCards.remove(Integer.valueOf(card1));
      
      int card1WithoutSuit;
    
      //assings the card without suit variable a number between 1 and 13 to account for the four different
      //suits
      if (card1 > 12){
        card1WithoutSuit = card1 % 13;
      }
      else{
      card1WithoutSuit = card1;
      }
      cardHand.add(card1WithoutSuit);
      
      card2 = removableCards.get(randomGenerator.nextInt(removableCards.size()));
      removableCards.remove(Integer.valueOf(card2));
      
      int card2WithoutSuit;
    
      if (card2 > 12){
        card2WithoutSuit = card2 % 13;
      }
      else{
      card2WithoutSuit = card2;
      }
      cardHand.add(card2WithoutSuit);
     
      card3 = removableCards.get(randomGenerator.nextInt(removableCards.size()));
      removableCards.remove(Integer.valueOf(card3));
      
      int card3WithoutSuit;
    
      if (card3 > 12){
        card3WithoutSuit = card3 % 13;
      }
      else{
      card3WithoutSuit = card3;
      }
      cardHand.add(card3WithoutSuit);
      
      card4 = removableCards.get(randomGenerator.nextInt(removableCards.size()));
      removableCards.remove(Integer.valueOf(card4));
      
      int card4WithoutSuit;
    
      if (card4 > 12){
        card4WithoutSuit = card4 % 13;
      }
      else{
      card4WithoutSuit = card4;
      }
      cardHand.add(card4WithoutSuit);
      
      card5 = removableCards.get(randomGenerator.nextInt(removableCards.size()));
      removableCards.remove(Integer.valueOf(card5));
      
      int card5WithoutSuit;
      
      if (card5 > 12){
        card5WithoutSuit = card5 % 13;
      }
      else{
      card5WithoutSuit = card5;
      }
      cardHand.add(card5WithoutSuit);
      
      //int variables to count how many times a card is the same and how many times a pair of cards occurs
      int counter = 0;
      int pairCounter = 0;
      
      //nested for loop to check for cards with the same number, checks every card after the first one and then
      //continues through the array list of cards
      for (int j = 0; j < cardHand.size(); j++){
        for (int k = j + 1; k < cardHand.size(); k++){
          //increments the counter if two cards have the same number
          if (cardHand.get(j) == cardHand.get(k)){
            counter++;
          }
        }
        //if the counter is 3, meaning 3 cards have the same number as one other card, the four of a kind
        //counter increments and the loop breaks
          if (counter == 3){
            fourOfAKind++;
            break;
          }
        //if the counter is 2, meaning 2 cards have the same number as one other card, the three of a kind
        //counter increments and the loop breaks
          else if (counter == 2){
            threeOfAKind++;
            break;
          }
        //if the counter is 1, the pairCounter variable increments
          else if (counter == 1){
            pairCounter++;
            //if the pair counter is 1, the one pair counter increments
            if (pairCounter == 1){
              onePair++;
            }
            //if the pair counter is 2, the two pair counter increments and the pair counter decrements
            else if (pairCounter == 2){
              twoPair++;
              onePair--;
            }
          }        
        //the counter resets for each new card
        counter = 0; 
      }    
    }
    
    //double variables for the probability of each hand
    double fourOfAKindProbability, threeOfAKindProbability, twoPairProbability, onePairProbability;
    
    //calculates the probablity of each type of hand, casts the division to a double
    fourOfAKindProbability = (double)fourOfAKind/numberOfHands;
    threeOfAKindProbability = (double)threeOfAKind/numberOfHands;
    twoPairProbability = (double)twoPair/numberOfHands;
    onePairProbability = (double)onePair/numberOfHands;
    
    //decimal format object with three decimal places
    DecimalFormat df = new DecimalFormat("###.###");
    
    System.out.println("The program looped through " + numberOfHands + " hands.");
    //prints out the probablities formatted to three decimal places
    System.out.println("The probability of getting a four of a kind is: " + df.format(fourOfAKindProbability));
    System.out.println("The probability of getting a three of a kind is: " + df.format(threeOfAKindProbability));
    System.out.println("The probability of getting two pair is: " + df.format(twoPairProbability));
    System.out.println("The probability of getting one pair is: " + df.format(onePairProbability));
  }
}
    