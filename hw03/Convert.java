//Jackson Nichols-Worley
//September 18th, 2018
//CSE 02 Converting Rainfall Data: Program that takes the amount of rain in an area and the 
//average rainfall, and computes the total amount of rainfall.

//imports scanner class
import java.util.Scanner;

public class Convert {
  public static void main (String args[]){
    //creates scanner object
    Scanner myScanner = new Scanner( System.in);
    
    //asks user to input affected area and creates variable for information
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = myScanner.nextDouble();
    
    //asks user to input average rainfall and creates variable for information
    System.out.print("Enter the average rainfall in the affected area in inches: ");
    double averageRainfall = myScanner.nextDouble();
    
    //cubic inches in an acre
    double cubicInchesPerAcre = 6272640.0;
    //converts inches per acre to gallons per acre
    double gallonsPerAcre = cubicInchesPerAcre/231;
    //formula for finding total amount of water, multiply affected area by average rainfall and gallons per
    //acre
    double gallonsOfWater = gallonsPerAcre * affectedArea * averageRainfall;
    //converts gallons to cubic miles
    double cubicMilesOfWater = gallonsOfWater * 0.000000000000908168597;
    
    //prints out amount of rainfall
    System.out.println("The ammount of rainfall in cubic miles is: " + cubicMilesOfWater);

  }
}  