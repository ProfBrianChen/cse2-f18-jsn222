//Jackson Nichols-Worley
//September 18th, 2018
//CSE 02 Finding Volume of a Pyramid: Program that takes the sqaure side and height of a pyramid
//and computes the volume of the pyramid. 

//imports scanner class
import java.util.Scanner;

public class Pyramid {
  public static void main (String args[]){
    //creates new scanner object
    Scanner myScanner = new Scanner( System.in );
  
    //asks user to input sqaure side of pyramid and creates variable for information
    System.out.print("Enter the length of the square side of the pyramid: ");
    double squareSideLength = myScanner.nextDouble();
    
    //asks user to input height of pyramid and creates variable for information
    System.out.print("Enter the height of the pyramid: ");
    double pyramidHeight = myScanner.nextDouble();
    
    //multiply square side length by sqaure side length to find the area of the base
    double baseArea = squareSideLength * squareSideLength;
    
    //volume of pyramid = lwh/3, base area is l multiplied by w for a sqaure pyramid
    double pyramidVolume = (baseArea * pyramidHeight)/3;
    
    //prints out volume of the pyramid
    System.out.println("The Volume inside the pyramid is: " + pyramidVolume);
  }
}
  
  