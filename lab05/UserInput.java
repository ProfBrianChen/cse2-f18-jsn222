//Jackson Nichols-Worley
//October 5th, 2018
//CSE 02 User Input: 

//imports scanner class, not built in to program
import java.util.Scanner;

public class UserInput {
  public static void main (String args[]){
    //creates new scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //boolean variable for testing the input
    boolean goodInput;
    
    //prompt to tell the user what the program is for
    System.out.println("Hello! We're going to talk about course information!");
    
    //int variable for the course number
    int courseNumber;
    
    //prompts the user to input the course number
    System.out.println("First, please enter the course number.");
    //checks whether the user inputed an integer
    goodInput = myScanner.hasNextInt();
    
    //while loop that continually prompts the user for an integer value
    while (!goodInput){
      System.out.println("Please enter an integer for the course number.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }
    
    //assigns courseNumber once user has inputed an integer
    courseNumber = myScanner.nextInt();
    
    //string variable for the department name
    String departmentName;
    
    //prompts the user to input the department name
    System.out.println("Next, please enter the department name.");
    //assigns departmentName to whatever the user inputs because any type will be converted to a string
    departmentName = myScanner.next();
    
    //int variable for number of times the class meets
    int numberOfMeetings;
    
    //prompts the user to input the number of times the class meets
    System.out.println("Next, please enter the number of times the class meets each week.");
    //checks whether the user inputed an integer
    goodInput = myScanner.hasNextInt();
    
    //while loop that continually prompts the user for an integer value
    while (!goodInput){
      System.out.println("Please enter an integer for the number of times the class meets.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }    
   
    //assigns numberOfMeetings once user has inputed an integer
    numberOfMeetings = myScanner.nextInt();
    
    //String variable for time the class starts
    String timeStart;
    
    //prompts the user to input the time the class starts
    System.out.println("Next, please enter the time that the class starts. First, enter the hour, and then enter the minutes");
    timeStart = myScanner.next();
    
    //String variable for intructor name
    String instructorName;
    
    //prompts the user to enter the intructor name
    System.out.println("Next, please enter the intructor's name.");
    //assigns instructor name to whatever the user inputs
    instructorName = myScanner.next();
    
    //int variable for people in the class
    int peopleInClass;
    
    //prompts the user to enter the number of people in the class
    System.out.println("Next, please enter the number of people in the class.");
    //checks whether the user entered an int
    goodInput = myScanner.hasNextInt();
    
    //while loop that continually prompts the user for an integer value
    while (!goodInput){
      System.out.println("Please enter an integer for the number of people in the class.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }    
    
    //assigns peopleInClass once user has inputed an integer
    peopleInClass = myScanner.nextInt();    
  }
}