//CSE 02 Welcome Class
public class WelcomeClass{
  public static void main (String args[]){
    //prints out welcome message, use new line of code for each line
    System.out.println("   -----------");
    System.out.println("   | Welcome |");
    System.out.println("   -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    //use two forward slashes because "/" has a special meaning                                     
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-J--S--N--2--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}