//imports scanner
import java.util.Scanner;

public class EncryptedX {
  public static void main (String args[]){
    Scanner myScanner = new Scanner( System.in );
    
    //declares boolean variable to check input
    boolean goodInput;
    
    //promtps user to input a number
    System.out.println("How many stars would you like to generate?");
    
    //declares int variable for number of rows
    int numberOfRows;
    
    //temp variable to check input
    int check; 
    
    //checks whether number is an int
    goodInput = myScanner.hasNextInt();
    
    //loops through until input is an int
    while (!goodInput){
      System.out.println("Please enter an integer for the number of stars.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }
    
    check = myScanner.nextInt();
    
    //loops through until input is between 1 and 100
    while (check > 100 || check < 2 || check % Math.sqrt(check) != 0){
      System.out.println("Please enter an integer for the number of stars that is a perfect square between 1 and 100.");
      check = myScanner.nextInt();
    }
    
    //assigns number of rows based on amount of stars
    numberOfRows = (int)Math.sqrt(check) + 1;
    
    //counts amount of times inner loop runs
    int counter = 1;
    int counter2 = numberOfRows;
    
    //for even number of rows
    if (numberOfRows % 2 != 0){
      //for loop, starts at 1, goes to number of rows
      for (int i = 1; i <= numberOfRows; i++){
        //nested for loop, starts at 1, goes to number of rows
        for (int j = 1; j <= numberOfRows; j++){
          //prints a space based on which row is currently being run through, works its way inward
          if (j == counter || j == counter2){
            System.out.print(" ");
          }
          else{
            System.out.print("*");
          }
        }
        System.out.println();
        //increments counters to move towards the middle
        counter++;
        counter2--;
      }
    }
    
    //for odd number of rows
    else{
      //for loop, starts at 1, goes to number of rows
      for (int i = 1; i <= numberOfRows; i++){
        //nested for loop, starts at 1, goes to number of rows
        if (i != numberOfRows/2){
          for (int j = 1; j <= numberOfRows; j++){
            //prints a space based on which row is currently being run through, works its way inward
            if (j == counter || j == counter2){
              System.out.print(" ");
            }
            else{
              System.out.print("*");
            }
          }
        }
        //removes one of the extra middle rows
        if (i != numberOfRows/2){
        System.out.println();
        }
        //increments counters to move towards the middle
        counter++;
        counter2--;
      }
    }
  }
}