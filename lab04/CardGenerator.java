//Jackson Nichols-Worley
//September 21st, 2018
//CSE 02 Card Generator: Picks a random card from a 52 card deck, accounts for four different
//suits and face cards.

//imports random class
import java.util.Random;

public class CardGenerator {
  public static void main (String args[]){
    
    //creates new random object
    Random randomGenerator = new Random();
    
    //initializes variables for card information
    String suitName;
    String cardIdentity;
    
    //creates random number, picks int from 0 to 51 and adds 1
    int number = randomGenerator.nextInt(51) + 1;
    
    //sets suit name, 13 cards in each suit then changes to next suit
    if (number < 14){
      suitName = "Diamonds";
    }
    else if (number < 27){
      suitName = "Clubs";
    }
    else if (number < 40){
      suitName = "Hearts";
    }
    else{
      suitName = "Spades";
    }
    
    //uses modulo operator to set number from 0 to 12, ignores suit, doesn't change number if it already is
    //in 0 to 12 range
    int numberWithoutSuit;
    
    if (number > 12){
      numberWithoutSuit = number % 13;
    }
    else{
      numberWithoutSuit = number;
    }
    
    //sets card identity
    //king is true for 0 because the reaminder of any multiple of 13 divided by 13 is 0
    if (numberWithoutSuit == 0){
      cardIdentity = "King";
    }
    //sets ace for number value of 1
    else if (numberWithoutSuit == 1){
      cardIdentity = "Ace";
    }
    //sets number cards, concatenates empty string with number value
    else if (numberWithoutSuit < 11){
      cardIdentity = "" + numberWithoutSuit;
    }
    //sets jack for number value of 11
    else if (numberWithoutSuit == 11){
      cardIdentity = "Jack";
    }
    //sets queen for number value of 12
    else if (numberWithoutSuit == 12){
      cardIdentity = "Queen";
    }
    //sets error for any other number value, should never be anything else but provides a catch
    else{
      cardIdentity = "Error";
    }

    //prints out the number and the suit of the card
    System.out.println("You picked the " + cardIdentity + " of " + suitName);
  }
}
  