//imports Arrays class
import java.util.Arrays; 
//imports java class
import java.util.Random;
//imports scanner class
import java.util.Scanner;

public class hw08 {
  public static void main (String args[]){
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    shuffle(cards); 
    printArray(cards); 
    System.out.println();
    while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      System.out.println();
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
  
  public static void printArray(String[] list){
    //for loop, runs through array list of strings
    for (int i = 0; i < list.length; i++){
      //prints out each index of the array, starting with zero and adds a space in between
      System.out.print(list[i] + " ");
    }
  }
  
  public static void shuffle(String[] list){
    //creates random object
    Random generator = new Random();
    
    //for loop, runs 1000 times
    for (int i = 0; i < 1000; i++){
      //random number from 1 to 52
      int num = generator.nextInt(51) + 1;
      
      //temp variable to hold the information at index 0 of the string array
      String temp = list[0];
      //sets index 0 of the the string array as the value of the index at num
      list [0] = list[num];
      //sets the index at num of the array as the value of temp which is holding the original value of index 0
      list[num] = temp;
    }
  }
  
  public static String[] getHand(String[] list, int index, int numCards){
    //String array variable for the card hand being returned
    String[] cardHand = new String[numCards];
      
    //if statement that shuffles the deck of cards and resets the index when the index is less than the number of cards being drawn
      if (numCards > index){
      shuffle(list);
      index = 51;
    }
    
    int count = 0;
    //for loop that starts at the index and goes to the spot in the array that is the index minus the number of cards 
    for (int i = index; i > index - numCards; i--){
      //sets the next index of the card hand array as the next last index of the deck of cards array
      cardHand[count] = list[i];
      count++;
    }
    return cardHand;
  }
}
