//imports scanner class
import java.util.Scanner;

public class PatternC {
  public static void main (String args[]){
    //declare and initializes scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //declares boolean variable to check input
    boolean goodInput;
    
    //promtps user to input a number
    System.out.println("How many rows would you like to generate?");
    
    //declares int variable for number of rows
    int numberOfRows;
    
    //checks whether number is an int
    goodInput = myScanner.hasNextInt();
    
    //loops through until input is an int
    while (!goodInput){
      System.out.println("Please enter an integer for the number of rows.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }
    
    numberOfRows = myScanner.nextInt();
    
    //loops through until input is between 1 and 10
    while (numberOfRows > 10 || numberOfRows < 2){
      System.out.println("Please enter an integer for the number of rows between 1 and 10.");
      numberOfRows = myScanner.nextInt();
    }
    
    //counts amount of times inner loop runs
    int counter = 0;
    
    //for loop, starts at 1, goes to number of rows
    for (int i = 1; i <= numberOfRows; i++){
      //nested for loop, starts at number of Rows, goes to 1
      for (int j = numberOfRows; j > 0; j--){
        //adds to the counter
        counter++;
        
        //prints out the current number in the loop and a space if the current number is less than the 
        //current row plus 1
        if (j < i + 1){
          System.out.print(j + " ");
        }
        //otherwise prints out a space to right justify the rows
        else{
          System.out.print("  ");
        }
       
      }
      //prints out a new line after each row
      System.out.println();
      //resets the counter for each row
      counter = 0;
    } 
  }
}