//imports scanner class
import java.util.Scanner;

public class PatternD {
  public static void main (String args[]){
    //declare and initializes scanner object
    Scanner myScanner = new Scanner( System.in );
    
    //declares boolean variable to check input
    boolean goodInput;
    
    //promtps user to input a number
    System.out.println("How many rows would you like to generate?");
    
    //declares int variable for number of rows
    int numberOfRows;
    
    //checks whether number is an int
    goodInput = myScanner.hasNextInt();
    
    //loops through until input is an int
    while (!goodInput){
      System.out.println("Please enter an integer for the number of rows.");
      myScanner.next();
      goodInput = myScanner.hasNextInt();
    }
    
    numberOfRows = myScanner.nextInt();
    
    //loops through until input is between 1 and 10
    while (numberOfRows > 10 || numberOfRows < 2){
      System.out.println("Please enter an integer for the number of rows between 1 and 10.");
      numberOfRows = myScanner.nextInt();
    }
    
    //counts amount of times inner loop runs
    int counter = 0;
    
    //for loop, starts at number of Rows, goes to 1
    for (int i = numberOfRows; i > 0; i--){
      //nested for loop, starts at number of Rows, goes to 1
      for (int j = numberOfRows; j > 0; j--){
        //adds to the counter
        counter++;
        
        //prints out the current number in the loop and a space, prints out the amount of numbers equal
        //to the row, starts at the highest number
        System.out.print(j + " ");
        
        //checks to make sure the counter is not bigger than the current row number
        if (counter == i){
          break;
        }
      }
      //prints out a new line after each row
      System.out.println();
      //resets the counter for each row
      counter = 0;
      //subtracts from the number of rows, removes the next leading number in each row
      numberOfRows--;
    }     
  }
}